# Useful-Bash-Commands

I am saving all useful bash commands (for me, on macOS) in this repository.

## Commands

### Common Bash Commands

<table>
  <tr>
    <th style=width:40%>Command</th>
    <th style=width:30%>Description</th>
    <th style=width:30%>Use Case</th>
  </tr>
  <tr>
    <td><code>!!</code></td>
    <td>Wenn Du das letzte Kommando wieder in das aktuelle Kommando einfügen willst</td>
    <td>-</td>
  </tr>
  <tr>
    <td><code>!&lthistory-nr&gt</code></td>
    <td>Wenn man ein bestimmtes Kommando mit einer Id aus der history ausführen will</td>
    <td>-</td>
  </tr>
  <tr>
    <td><code>&ltcommand1&gt&#x3B &ltcommand*n&gt </code></td>
    <td>Kommandoverkettung</td>
    <td>Wenn man will, dass alle Kommandos ausgeführt werden obwohl in der Kette ein Fehler ist</td>
  </tr>
  <tr>
    <td><code>&ltcommand1&gt&amp &ltcommand*n&gt </code></td>
    <td>Kommandoverkettung</td>
    <td>Wenn man nicht will, dass alle Kommandos ausgeführt werden wenn in der Kette ein Fehler ist</td>
  </tr>
  <tr>
    <td><code>truncate -s &ltlines&gt</code></td>
    <td>Löscht die Daten einer Datei auf angegebene Anzahl of lines</td>
    <td>Wenn man eine Logfile nicht Löschen will, aber diese trotzdem leeren will</td>
  </tr>

</table>

### Bash Directory Navigation

<table>
  <tr>
    <th style=width:40%>Command</th>
    <th style=width:30%>Description</th>
    <th style=width:30%>Use Case</th>
  </tr>
  <tr>
    <td><code>popd</code></td>
    <td>Damit kann man wieder zu dem Directory zurück wo man zuvor war</td>
    <td>Wenn man mit cd in ein komplett anderes Verzeichnis wechselt, dann kommt man mit popd wieder genau in das
      Verzeichnis zurück von dem man rausgewechselt ist</td>
  </tr>
</table>

### Git

<table>
  <tr>
    <th style=width:40%>Command</th>
    <th style=width:30%>Description</th>
    <th style=width:30%>Use Case</th>
  </tr>
  <tr>
    <td><code>git clean -dfX</code></td>
    <td>Alle Files die nicht im Repository sind löschen</td>
    <td>Man will alle Files die bspw. in der .gitignore angegeben sind löschen, sodass das lokale Repo den gleichen
      Stand hat als das online Repo</td>
  </tr>
  <tr>
    <td><code>git reset --soft HEAD~1</code></td>
    <td> Zurückziehen eines Commits und gleichzeitiges übertragen der Commiteten Files in Stage. </td>
    <td>Zu spät gemerkt, dass Du alles ausversehen geaddet und dann Commiteted hast.</td>
  </tr>
  <tr>
    <td><code>git push origin -u HEAD</code></td>
    <td>Den aktuellen Branch auf den Remote Pushen, wenn noch nicht vorhanden dann erstellen</td>
    <td>Du hast lokal einen Branch angelegt und willst ihn jetzt Pushen, dann einfach diesen Command ausführen</td>
  </tr>


</table>

### SSH

<table>
  <tr>
    <th style=width:40%>Command</th>
    <th style=width:30%>Description</th>
    <th style=width:30%>Use Case</th>
  </tr>
  <tr>
    <td><code>ssh-add --apple-use-keychain</code></td>
    <td>Passphrase für das System als Default festlegen und im Keychain speichern</td>
    <td>Wenn man bspw. ein Git-Repository per SSH ausgecheckt hat, dann kann man ganz simple dafür sorgen, dass die
      Passphrase nicht jedes mal bei der Kommunikation wieder abgefragt wird </td>
  </tr>
</table>

### Netzwerk

<table>
  <tr>
    <th style=width:40%>Command</th>
    <th style=width:30%>Description</th>
    <th style=width:30%>Use Case</th>
  </tr>
  <tr>
    <td><code>sudo lsof -i -P | grep -i LISTEN</code></td>
    <td>Offene Ports finden</td>
    <td>-</td>
  </tr>
</table>

## System

<table>
  <tr>
    <th style=width:40%>Command</th>
    <th style=width:30%>Description</th>
    <th style=width:30%>Use Case</th>
  </tr>
  <tr>
    <td><code>kill &ltpid&gt</code></td>
    <td>Prozess beenden</td>
    <td>-</td>
  </tr>
</table>

<!--
<table>
<tr>
<th style=width:40%>Command</th>
<th style=width:30%>Description</th>
<th style=width:30%>Use Case</th>
</tr>
<tr>
<td><code></code></td>
<td></td>
<td></td>
</tr>
</table>
--->